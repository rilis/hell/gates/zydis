## Zydis proxy repository for hell

This is proxy repository for [Zydis library](https://zydis.re/), which allow you to build and install it  using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of Zydis using hell, have improvement idea, or want to request support for other versions of Zydis, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in Zydis itself please create issue on [Zydis issue tracker](https://github.com/zyantific/zydis/issues), because here we don't do any kind of Zydis development.
